# KiCad-RP-Pico

A 3D footprint of the Raspberry Pi Pico board for KiCad.
To remove the need for git submodules and thus possibly broken KiCad projects, the symbol lib was copied to this repository.
The original project source can be found [over here][src].
A tweet with an image of your Raspberry Pi Pico project with a link [to the original project][src] and author (@tpcware) would be greatly appreciated.

[src]: https://github.com/ncarandini/KiCad-RP-Pico/tree/main
